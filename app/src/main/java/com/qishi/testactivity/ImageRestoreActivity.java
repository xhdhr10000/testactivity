package com.qishi.testactivity;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Author: xhdhr10000
 * Date: 16/3/17
 */
public class ImageRestoreActivity extends AppCompatActivity {
    private static final String TAG = "ImageRestoreActivity";

    private ImageView ivPhoto;
    private RectView rvPhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_image_restore);

        Bundle extras = this.getIntent().getExtras();
        if (extras == null) {
            Log.e(TAG, "No extras");
            this.finish();
            return;
        }
        final int image = extras.getInt("image", -1);
        if (image == -1) {
            Log.e(TAG, "No image specified");
            this.finish();
            return;
        }
        ivPhoto = (ImageView) this.findViewById(R.id.ivPhoto);
//        ivPhoto.setImageResource(image);
        ivPhoto.post(new Runnable() {
            @Override
            public void run() {
                ivPhoto.setImageBitmap(BitmapUtil.readBitmapResource(getResources(), image,
                        ivPhoto.getWidth(), ivPhoto.getHeight()));
            }
        });
        rvPhoto = (RectView) this.findViewById(R.id.svPhoto);
    }

    public void onClickConfirm(View v) {
        float [] m = new float[9];
        ivPhoto.getImageMatrix().getValues(m);
        Bitmap bmp = rvPhoto.confirm(
                ((BitmapDrawable)ivPhoto.getDrawable()).getBitmap(),
                ivPhoto.getDrawable().getIntrinsicWidth(),
                ivPhoto.getDrawable().getIntrinsicHeight(),
                m[Matrix.MSCALE_X], m[Matrix.MSCALE_Y],
                m[Matrix.MTRANS_X], m[Matrix.MTRANS_Y]);
        ivPhoto.setImageBitmap(bmp);
    }

    public void onClickCancel(View v) {
        this.finish();
    }
}
