package com.qishi.testactivity;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("Fragment", "~~~onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("Fragment", "~~~onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("Fragment", "~~~onCreateView");
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e("Fragment", "~~~onViewCreated");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("Fragment", "~~~onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("Fragment", "~~~onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Fragment", "~~~onResume");
    }

    @Override
    public void onPause() {
        Log.e("Fragment", "~~~onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e("Fragment", "~~~onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.e("Fragment", "~~~onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.e("Fragment", "~~~onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.e("Fragment", "~~~onDetach");
        super.onDetach();
    }

    @Override
    public boolean getAllowEnterTransitionOverlap() {
        return super.getAllowEnterTransitionOverlap();
    }
}
