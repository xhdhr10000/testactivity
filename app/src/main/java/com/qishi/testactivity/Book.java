package com.qishi.testactivity;

import com.orm.SugarRecord;

/**
 * Created by xhdhr10000 on 16/3/3.
 */
public class Book extends SugarRecord {
    String title;
    String edition;

    public Book(String edition, String title) {
        this.edition = edition;
        this.title = title;
    }

    public Book() {
    }
}
