package com.qishi.testactivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.ImageView;

/**
 * Author: xhdhr10000
 * Date: 16/3/24
 */
public class AnimActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//        getWindow().setExitTransition(new Explode());
        this.setContentView(R.layout.activity_anim);
    }

    public void onToggle(View v) {
        final ImageView image = (ImageView) this.findViewById(R.id.image1);

        if (image.getVisibility() == View.INVISIBLE) {
            // get the center for the clipping circle
            int cx = image.getWidth() / 2;//(image.getLeft() + image.getRight()) / 2;
            int cy = image.getHeight() / 2;//(image.getTop() + image.getBottom()) / 2;

            // get the final radius for the clipping circle
            int finalRadius = Math.max(image.getWidth(), image.getHeight());

            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(image, cx, cy, 0, finalRadius);

            // make the view visible and start the animation
            image.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            // get the center for the clipping circle
            int cx = image.getWidth() / 2;//(image.getLeft() + image.getRight()) / 2;
            int cy = image.getHeight() / 2;//(image.getTop() + image.getBottom()) / 2;

            // get the initial radius for the clipping circle
            int initialRadius = image.getWidth();

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(image, cx, cy, initialRadius, 0);

            // make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    image.setVisibility(View.INVISIBLE);
                }
            });

            // start the animation
            anim.start();
        }
    }

    public void onTransition(View v) {
        ImageView image2 = (ImageView) this.findViewById(R.id.image2);
        ImageView image3 = (ImageView) this.findViewById(R.id.image3);
        Intent intent = new Intent(this, TransitionActivity.class);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
                Pair.create((View) image2, "image"), Pair.create((View) image3, "image1"));
        startActivity(intent, options.toBundle());
    }
}
