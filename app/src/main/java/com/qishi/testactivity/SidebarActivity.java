package com.qishi.testactivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Author: xhdhr10000
 * Date: 16/3/23
 */
public class SidebarActivity extends AppCompatActivity {
    private String[] items = {
            "item1",
            "item2",
            "item3",
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_sidebar);

        ListView sidebar = (ListView) this.findViewById(R.id.sidebar);
        if (sidebar != null) {
            sidebar.setAdapter(new ArrayAdapter<>(this, R.layout.sidebar_item, items));
        }
    }
}
