package com.qishi.testactivity;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Author: xhdhr10000
 * Date: 16/3/15
 */
public class SquareImageView extends ImageView {
    private boolean measureWidth;

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public SquareImageView(Context context) {
        super(context);
    }

    public void setMeasure(boolean width) {
        measureWidth = width;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (measureWidth)
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        else
            super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}
