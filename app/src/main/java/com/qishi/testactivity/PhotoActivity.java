package com.qishi.testactivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Author: xhdhr10000
 * Date: 16/3/10
 */
public class PhotoActivity extends AppCompatActivity {
    private static final String TAG = "PhotoActivity";
    private Toolbar toolbar;

    private ArrayList<Integer> alPhotoSend;
    private TextView tvPhotoSendTitle;
    private String sendTitle;
    private RecyclerView rvPhotoSend;
    private PhotoGridLayoutManager lmPhotoSend;

    private ArrayList [] alPhotoRecv;
    private TextView [] tvPhotoRecvTitle;
    private String [] recvTitle;
    private RecyclerView [] rvPhotoRecv;
    private PhotoGridLayoutManager [] lmPhotoRecv;
    private int recvCount;

    private int spanCount;
    private int savedOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        savedOrientation = this.getResources().getConfiguration().orientation;

        /* Get intent extras */
        Bundle extras = this.getIntent().getExtras();
        if (extras == null) {
            Log.e(TAG, "Intent has no extras");
            this.finish();
            return;
        }
        sendTitle = extras.getString("sendTitle");
        if (sendTitle == null) {
            Log.e(TAG, "No title for photo send");
            this.finish();
            return;
        }
        alPhotoSend = extras.getIntegerArrayList("alPhotoSend");
        if (alPhotoSend == null) {
            Log.e(TAG, "No photo send");
            this.finish();
            return;
        }
        recvCount = extras.getInt("recvCount", 0);
        if (recvCount == 0) {
            Log.e(TAG, "Receiver count is 0");
            this.finish();
            return;
        }
        recvTitle = new String[recvCount];
        alPhotoRecv = new ArrayList[recvCount];
        for (int i=0; i<recvCount; i++) {
            recvTitle[i] = extras.getString("recvTitle"+i);
            if (recvTitle[i] == null) {
                Log.e(TAG, "No title for photo recv[" + i + "]");
                recvTitle[i] = "";
            }
            alPhotoRecv[i] = extras.getIntegerArrayList("alPhotoRecv");
            if (alPhotoRecv[i] == null) {
                Log.e(TAG, "No photo recv[" + i + "]");
                alPhotoRecv[i] = new ArrayList<Integer>();
            }
        }

        /* Initialize view */
        toolbar = (Toolbar) this.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.photo);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(true);

        View v = this.findViewById(R.id.photoSend);
        tvPhotoSendTitle = (TextView) v.findViewById(R.id.tvPhotoTitle);
        tvPhotoSendTitle.setText(sendTitle);
        rvPhotoSend = (RecyclerView) v.findViewById(R.id.rvPhoto);

        LinearLayout ll = (LinearLayout)this.findViewById(R.id.llPhotoRecv);
        int margin = getResources().getDimensionPixelSize(R.dimen.photo_margin);
        int width = getResources().getDimensionPixelSize(R.dimen.photo_container_size);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.setMargins(margin, margin, margin, margin);
        tvPhotoRecvTitle = new TextView[recvCount];
        rvPhotoRecv  = new RecyclerView[recvCount];
        for (int i=0; i<recvCount; i++) {
            v = this.getLayoutInflater().inflate(R.layout.photo_container, (ViewGroup)v, false);
            ll.addView(v, lp);
            tvPhotoRecvTitle[i] = (TextView) v.findViewById(R.id.tvPhotoTitle);
            tvPhotoRecvTitle[i].setText(recvTitle[i]);
            rvPhotoRecv[i] = (RecyclerView) v.findViewById(R.id.rvPhoto);
        }

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            spanCount = 3;
        else
            spanCount = 2;

        lmPhotoSend = new PhotoGridLayoutManager(this, spanCount, LinearLayoutManager.HORIZONTAL, false);
        rvPhotoSend.setLayoutManager(lmPhotoSend);
        rvPhotoSend.setAdapter(new PhotoAdapter(alPhotoSend));
        rvPhotoSend.addOnItemTouchListener(new OnItemTouchListener());
        rvPhotoSend.setOnDragListener(new OnDragListener());

        lmPhotoRecv = new PhotoGridLayoutManager[recvCount];
        for (int i=0; i<recvCount; i++) {
            lmPhotoRecv[i] = new PhotoGridLayoutManager(this, spanCount, LinearLayoutManager.HORIZONTAL, false);
            rvPhotoRecv[i].setLayoutManager(lmPhotoRecv[i]);
            rvPhotoRecv[i].setAdapter(new PhotoAdapter(alPhotoRecv[i]));
            rvPhotoRecv[i].addOnItemTouchListener(new OnItemTouchListener());
            rvPhotoRecv[i].setOnDragListener(new OnDragListener());
        }

//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
        if (!OpenCVLoader.initDebug()) {
            Log.e(TAG, "OpenCVLoader.initDebug failed");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation != savedOrientation) {
            savedOrientation = newConfig.orientation;
            if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
                spanCount = 3;
            else
                spanCount = 2;

            lmPhotoSend.setSpanCount(spanCount);
            for (PhotoGridLayoutManager aLmPhotoRecv : lmPhotoRecv)
                aLmPhotoRecv.setSpanCount(spanCount);
        }
    }

    class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
        private ArrayList<Integer> photo;

        @Override
        public long getItemId(int position) {
            return photo.get(position).hashCode();
        }

        public PhotoAdapter(ArrayList<Integer> photo) {
            super();
            this.photo = photo;
            this.setHasStableIds(true);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(PhotoActivity.this).inflate(R.layout.photo_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            ImageView iv = (ImageView)holder.view.findViewById(R.id.ivPhoto);
            iv.post(new Runnable() {
                @Override
                public void run() {
                    ImageView iv = (ImageView)holder.view.findViewById(R.id.ivPhoto);
                    int position = holder.getAdapterPosition();
                    iv.setImageBitmap(BitmapUtil.readBitmapResource(getResources(), photo.get(position),
                            iv.getWidth(), iv.getHeight()));
                }
            });
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PhotoActivity.this, ImageRestoreActivity.class);
                    intent.putExtra("image", photo.get(holder.getAdapterPosition()));
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return photo.size();
        }

        public ArrayList<Integer> getData() {
            return photo;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            SquareImageView view;
            public ViewHolder(View itemView) {
                super(itemView);
                view = (SquareImageView)itemView;
                view.setMeasure(false);
            }
        }
    }

    class PhotoLinearLayoutManager extends LinearLayoutManager {
        @Override
        public boolean supportsPredictiveItemAnimations() {
            return true;
        }

        public PhotoLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }
    }

    class PhotoGridLayoutManager extends GridLayoutManager {
        public PhotoGridLayoutManager(Context context, int spanCount, int orientation, boolean reverseLayout) {
            super(context, spanCount, orientation, reverseLayout);
        }

        @Override
        public boolean supportsPredictiveItemAnimations() {
            return true;
        }
    }

    class DragData {
        ArrayList<Integer> sender;
        int index;
    }

    class OnItemTouchListener implements RecyclerView.OnItemTouchListener {
        private float mx, my;
        private View photo;

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mx = e.getX();
                    my = e.getY();
                    Rect r = new Rect();
                    int i;
                    for (i=0; i<rv.getChildCount(); i++) {
                        rv.getChildAt(i).getHitRect(r);
                        if (r.contains((int)mx, (int)my)) {
                            photo = rv.getChildAt(i);
                            break;
                        }
                    }
                    if (i == rv.getChildCount())
                        mx = my = -1;
                    break;
                case MotionEvent.ACTION_MOVE: {
                    if (mx == -1 || my == -1) break;
                    float dx = Math.abs(e.getX() - mx);
                    float dy = Math.abs(e.getY() - my);
                    if (dx > 50 || dy > 50) {
                        if (dy / dx > 1.2) {
                            PhotoAdapter pa = (PhotoAdapter)rv.getAdapter();
                            PhotoAdapter.ViewHolder vh = (PhotoAdapter.ViewHolder)rv.getChildViewHolder(photo);
                            DragData data = new DragData();
                            data.sender = pa.getData();
                            data.index = vh.getAdapterPosition();

                            vh.view.startDrag(null, new View.DragShadowBuilder(vh.view), data, 0);
                        } else if (dy / dx < 0.5)
                            mx = my = -1;
                    }
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    mx = my = -1;
                    break;
                }
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) { }
    }

    class OnDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            RecyclerView rv = (RecyclerView) v;

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    return true;
                case DragEvent.ACTION_DROP:
                    float x = event.getX();
                    float y = event.getY();
                    Rect r = new Rect();
                    int pos = rv.getChildCount();
                    for (int i=0; i<rv.getChildCount(); i++) {
                        View child = rv.getChildAt(i);
                        child.getHitRect(r);
                        if (x > r.right) continue;
                        pos = Math.min(i+spanCount, rv.getChildCount())-1;
                        pos = rv.getChildViewHolder(rv.getChildAt(pos)).getAdapterPosition()+1;
                        for (int j=i; j<i+spanCount && j<rv.getChildCount(); j++) {
                            child = rv.getChildAt(j);
                            child.getHitRect(r);
                            if (y < r.centerY()) {
                                pos = rv.getChildViewHolder(child).getAdapterPosition();
                                break;
                            }
                        }
                        break;
                    }

                    DragData data = (DragData) event.getLocalState();
                    PhotoAdapter pa = (PhotoAdapter) rv.getAdapter();
                    pa.getData().add(pos, data.sender.get(data.index));
                    if (pa.getData() == data.sender)
                        data.sender.remove(data.index + ((pos>data.index)?0:1));
                    else
                        data.sender.remove(data.index);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    rv.getAdapter().notifyDataSetChanged();
                    break;
            }
            return false;
        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
}
