package com.qishi.testactivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView lvLeftMenu;
    private String[] lvs = {"List Item 01", "List Item 02", "List Item 03", "List Item 04"};
    private ArrayAdapter arrayAdapter;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        rootView = mDrawerLayout;
        toolbar.setTitle("Toolbar");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lvs);
        lvLeftMenu.setAdapter(arrayAdapter);

        RecyclerView rvMain = (RecyclerView) findViewById(R.id.lv_main);
        rvMain.setLayoutManager(new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL));
        CardAdapter adapter = new CardAdapter();
        rvMain.setAdapter(adapter);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        Log.e("Activity", "onCreate~~~");

        ImageView iv = (ImageView) findViewById(R.id.ivPhoto);
        Log.e("XX", iv + "");
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;

        public ViewHolder(LinearLayout itemView) {
            super(itemView);
            linearLayout = itemView;
        }
    }

    private void findViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.dl_left);
        lvLeftMenu = (ListView) findViewById(R.id.lv_left_menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Activity", "onStart~~~");
    }

    @Override
    protected void onStop() {
        Log.e("Activity", "onStop~~~");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e("Activity", "onDestroy~~~");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.e("Activity", "onPause~~~");
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("Activity", "onRestart~~~");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Activity", "onResume~~~");
        Book book = new Book("123", "321");
        book.save();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_anim) {
            startActivity(new Intent(this, AnimActivity.class));
        } else if (id == R.id.action_input) {
            startActivity(new Intent(this, DigitInputActivity.class));
        } else if (id == R.id.action_crossfade) {
            startActivity(new Intent(this, CrossfadeActivity.class));
        } else if (id == R.id.action_gif) {
            startActivity(new Intent(this, GifActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final String text = "Item " + String.valueOf(position);
            CardView cardView = (CardView) holder.itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Snackbar.make(rootView, text, Snackbar.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
                    intent.putExtra("sendTitle", "照片库");
                    ArrayList<Integer> alPhotoSend = new ArrayList<>();
                    alPhotoSend.add(R.drawable.galaxy);
                    alPhotoSend.add(R.drawable.test_image);
                    alPhotoSend.add(R.drawable.imag0400);
                    alPhotoSend.add(R.drawable.img_20160321_201618);
                    alPhotoSend.add(R.drawable.img_20160321_201651);
                    alPhotoSend.add(R.drawable.img_20160321_201727);
                    alPhotoSend.add(R.drawable.file1);
                    alPhotoSend.add(R.drawable.file2);
                    alPhotoSend.add(R.drawable.file3);
                    alPhotoSend.add(R.drawable.file4);
                    alPhotoSend.add(R.drawable.file5);
                    alPhotoSend.add(R.drawable.file6);
                    alPhotoSend.add(R.drawable.file7);
                    alPhotoSend.add(R.drawable.d1);
                    alPhotoSend.add(R.drawable.d2);
                    alPhotoSend.add(R.drawable.d3);
                    alPhotoSend.add(R.drawable.d4);
                    alPhotoSend.add(R.drawable.d5);
                    alPhotoSend.add(R.drawable.d6);
                    alPhotoSend.add(R.drawable.d7);
                    alPhotoSend.add(R.drawable.d8);
                    alPhotoSend.add(R.drawable.d9);
                    alPhotoSend.add(R.drawable.d10);
                    alPhotoSend.add(R.drawable.d11);
                    alPhotoSend.add(R.drawable.d12);
                    alPhotoSend.add(R.drawable.d13);
                    alPhotoSend.add(R.drawable.d14);
                    alPhotoSend.add(R.drawable.d15);
                    alPhotoSend.add(R.drawable.d16);
                    alPhotoSend.add(R.drawable.d17);
                    alPhotoSend.add(R.drawable.d19);
                    alPhotoSend.add(R.drawable.d20);
                    alPhotoSend.add(R.drawable.d21);
                    intent.putExtra("alPhotoSend", alPhotoSend);
                    intent.putExtra("recvCount", holder.getAdapterPosition());
                    for (int i = 0; i < holder.getAdapterPosition(); i++) {
                        intent.putExtra("recvTitle" + i, "照片" + i);
                    }
                    startActivity(intent);
                }
            });

            int diff = position / 5 * 0x20;
            int color = Color.rgb(0x3c + diff, 0xb6 - diff, 0xe4 - diff);
            cardView.setCardBackgroundColor(color);

            TextView tv = (TextView) cardView.findViewById(R.id.cardTitle);
            tv.setText(text);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LinearLayout ll = (LinearLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_cardview, parent, false);
            return new ViewHolder(ll);
        }

        @Override
        public int getItemCount() {
            return 30;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            View itemView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.itemView = itemView;
            }
        }
    }
}
