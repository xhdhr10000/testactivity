package com.qishi.testactivity;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Author: xhdhr10000
 * Date: 16/4/8
 */
public class DigitInputPanel extends RelativeLayout {
    private Callback callback;
    private RelativeLayout root;

    public DigitInputPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    protected void initView() {
        root = (RelativeLayout) LayoutInflater.from(getContext()).
                inflate(R.layout.digit_input_panel, this, true);
        if (root == null) return;

        root.findViewById(R.id.iDot).setOnTouchListener(onInput);
        root.findViewById(R.id.iEqual).setOnTouchListener(onInput);
        root.findViewById(R.id.i0).setOnTouchListener(onInput);
        root.findViewById(R.id.i1).setOnTouchListener(onInput);
        root.findViewById(R.id.i2).setOnTouchListener(onInput);
        root.findViewById(R.id.i3).setOnTouchListener(onInput);
        root.findViewById(R.id.i4).setOnTouchListener(onInput);
        root.findViewById(R.id.i5).setOnTouchListener(onInput);
        root.findViewById(R.id.i6).setOnTouchListener(onInput);
        root.findViewById(R.id.i7).setOnTouchListener(onInput);
        root.findViewById(R.id.i8).setOnTouchListener(onInput);
        root.findViewById(R.id.i9).setOnTouchListener(onInput);

        root.findViewById(R.id.iC).setOnTouchListener(onInput);
        root.findViewById(R.id.iBackspace).setOnTouchListener(onInput);
        root.findViewById(R.id.iLeftBracket).setOnTouchListener(onInput);
        root.findViewById(R.id.iRightBracket).setOnTouchListener(onInput);
        root.findViewById(R.id.iDivide).setOnTouchListener(onInput);
        root.findViewById(R.id.iPlus).setOnTouchListener(onInput);
        root.findViewById(R.id.iMinus).setOnTouchListener(onInput);
        root.findViewById(R.id.iAdd).setOnTouchListener(onInput);

        root.findViewById(R.id.iHundred).setOnTouchListener(onInput);
        root.findViewById(R.id.iThousand).setOnTouchListener(onInput);
        root.findViewById(R.id.iTenThousand).setOnTouchListener(onInput);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private OnTouchListener onInput = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                callback.onInput(((TextView) v).getText().toString());
            return false;
        }
    };

    public interface Callback {
        void onInput(String input);
    }
}
