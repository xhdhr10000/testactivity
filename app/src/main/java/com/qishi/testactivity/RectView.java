package com.qishi.testactivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Author: xhdhr10000
 * Date: 16/3/17
 */
public class RectView extends View {
    private Paint paint;
    private Point[] vertex;
    private Point viewSize;
    private boolean inited = false;

    public RectView(Context context) {
        super(context);
    }

    public RectView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RectView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RectView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void init() {
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(this.getContext(), R.color.colorAccent));
        paint.setStrokeWidth(3);

        Rect rect = new Rect();
        this.getGlobalVisibleRect(rect);
        viewSize = new Point(rect.right - rect.left, rect.bottom - rect.top);

        vertex = new Point[4];
        vertex[0] = new Point(viewSize.x/6, viewSize.y/6);
        vertex[1] = new Point(viewSize.x*5/6, viewSize.y/6);
        vertex[2] = new Point(viewSize.x*5/6, viewSize.y*5/6);
        vertex[3] = new Point(viewSize.x/6, viewSize.y*5/6);

        this.setOnTouchListener(new OnTouchListener());

        inited  = true;
    }

    public Bitmap confirm(Bitmap bmp, float ox, float oy, float sx, float sy, float tx, float ty) {
        double w1 = Math.sqrt(Math.pow(vertex[2].x - vertex[3].x, 2) * 2);
        double w2 = Math.sqrt(Math.pow(vertex[1].x - vertex[0].x, 2) * 2);
        double h1 = Math.sqrt(Math.pow(vertex[1].y - vertex[2].y, 2) * 2);
        double h2 = Math.sqrt(Math.pow(vertex[0].y - vertex[3].y, 2) * 2);
        double maxWidth = (w1 > w2) ? w1 : w2;
        double maxHeight = (h1 > h2) ? h1 : h2;

        Mat src = new Mat(4, 1, CvType.CV_32FC2);
        src.put(0, 0,
                (vertex[0].x - tx) / sx, (vertex[0].y - ty) / sy,
                (vertex[1].x - tx) / sx, (vertex[1].y - ty) / sy,
                (vertex[2].x - tx) / sx, (vertex[2].y - ty) / sy,
                (vertex[3].x - tx) / sx, (vertex[3].y - ty) / sy);

        Mat dst = new Mat(4, 1, CvType.CV_32FC2);
        dst.put(0, 0,
                0, 0,
                maxWidth - 1, 0,
                maxWidth - 1, maxHeight - 1,
                0, maxHeight - 1);

        Mat cvMat = new Mat((int) ox, (int) oy, CvType.CV_8UC4);
        Utils.bitmapToMat(bmp, cvMat);
        Mat cvMatTest = new Mat();
        cvMat.copyTo(cvMatTest);
//        Core.transpose(cvMat, cvMatTest);
        cvMat.release();
//        Core.flip(cvMatTest, cvMatTest, 1);
        Mat original = cvMatTest;
        Mat undistorted = new Mat(new Size(maxWidth, maxHeight), CvType.CV_8UC1);
        Imgproc.warpPerspective(original, undistorted, Imgproc.getPerspectiveTransform(dst, src), new Size(maxWidth, maxHeight), Imgproc.WARP_INVERSE_MAP);
        original.release();

        Bitmap outBmp = Bitmap.createBitmap(undistorted.width(), undistorted.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(undistorted, outBmp);
        return outBmp;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w * h != 0 && !inited) init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawLine((float)vertex[0].x, (float)vertex[0].y, (float)vertex[1].x, (float)vertex[1].y, paint);
        canvas.drawLine((float)vertex[1].x, (float)vertex[1].y, (float)vertex[2].x, (float)vertex[2].y, paint);
        canvas.drawLine((float)vertex[2].x, (float)vertex[2].y, (float)vertex[3].x, (float)vertex[3].y, paint);
        canvas.drawLine((float)vertex[3].x, (float)vertex[3].y, (float)vertex[0].x, (float)vertex[0].y, paint);
    }

    public class OnTouchListener implements View.OnTouchListener {
        private float x = -1, y = -1;
        private int point = -1;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    x = event.getX();
                    y = event.getY();
                    point = 0;
                    double d = 2147483647;
                    for (int i=0; i<4; i++) {
                        double dd = Math.sqrt(Math.pow(x - vertex[i].x, 2) + Math.pow(y - vertex[i].y, 2));
                        if (dd < d) {
                            point = i;
                            d = dd;
                        }
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (point == -1 || x == -1 || y == -1) break;
                    float dx = event.getX() - x;
                    float dy = event.getY() - y;
                    vertex[point].x += dx;
                    if (vertex[point].x < 0) vertex[point].x = 0;
                    if (vertex[point].x > viewSize.x) vertex[point].x = viewSize.x;
                    vertex[point].y += dy;
                    if (vertex[point].y < 0) vertex[point].y = 0;
                    if (vertex[point].y > viewSize.y) vertex[point].y = viewSize.y;
                    x = event.getX();
                    y = event.getY();
                    break;
                case MotionEvent.ACTION_UP:
                    point = -1;
                    x = y = -1;
                    break;
            }
            RectView.this.invalidate();
            return true;
        }
    }
}
