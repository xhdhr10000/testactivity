package com.qishi.testactivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Author: xhdhr10000
 * Date: 16/4/8
 */
public class DigitInputActivity extends AppCompatActivity {
    private TextView tv1;
    private DigitInputPanel dip;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digitinput);
        tv1 = (TextView) findViewById(R.id.tv1);
        dip = (DigitInputPanel) findViewById(R.id.dip);
        dip.setCallback(new DigitInputPanel.Callback() {
            @Override
            public void onInput(String input) {
                String text = (String) tv1.getText();
                if (input.equals(getString(R.string.dipClear))) {
                    text = null;
                } else if (input.equals(getString(R.string.dipBackspace))) {
                    if (text.length() > 0)
                        text = text.substring(0, text.length() - 1);
                } else if (input.equals(getString(R.string.dipHundred))) {
                    text += "00";
                } else if (input.equals(getString(R.string.dipThousand))) {
                    text += "000";
                } else if (input.equals(getString(R.string.dipTenThousand))) {
                    text += "0000";
                } else if (input.equals(getString(R.string.dipEqual))) {
                    text += "=" + calculate(text);
                } else {
                    text += input;
                }

                tv1.setText(text);
            }
        });
    }

    public String calculate(String expression) {
        try {
            ArrayList postfix = transform(expression);
            return calculate(postfix).toPlainString();
        } catch (Exception e) {
            e.printStackTrace();
            return "非法表达式";
        }
    }

    public static ArrayList transform(String prefix) {
        int i, len = prefix.length();

        prefix = prefix + '#';
        Stack<Character> stack = new Stack<>();
        stack.push('#');
        ArrayList postfix = new ArrayList();

        for (i = 0; i < len + 1; i++) {
            if (Character.isDigit(prefix.charAt(i)) || prefix.charAt(i) == '.') {
                int j;
                for (j = i; j < len + 1; j++)
                    if (!Character.isDigit(prefix.charAt(j)) && prefix.charAt(j) != '.')
                        break;
                postfix.add(new BigDecimal(prefix.substring(i, j)));
                i = j - 1;
            } else {
                if (i == 0) postfix.add(new BigDecimal(0));
                switch (prefix.charAt(i)) {
                    case '(':
                        stack.push(prefix.charAt(i));
                        break;
                    case ')':
                        while (stack.peek() != '(') {
                            postfix.add(stack.pop());
                        }
                        stack.pop();
                        break;
                    default:
                        while (stack.peek() != '#'
                                && compare(stack.peek(), prefix.charAt(i))) {
                            postfix.add(stack.pop());
                        }
                        if (prefix.charAt(i) != '#') {
                            stack.push(prefix.charAt(i));
                        }
                        break;
                }
            }
        }
        return postfix;
    }

    public static boolean compare(char peek, char cur) {
        if (peek == '*'
                && (cur == '+' || cur == '-' || cur == '/' || cur == '*')) {
            return true;
        } else if (peek == '/'
                && (cur == '+' || cur == '-' || cur == '*' || cur == '/')) {
            return true;
        } else if (peek == '+' && (cur == '+' || cur == '-')) {
            return true;
        } else if (peek == '-' && (cur == '+' || cur == '-')) {
            return true;
        } else if (cur == '#') {
            return true;
        }
        return false;
    }

    public static BigDecimal calculate(ArrayList postfix) {
        BigDecimal res = new BigDecimal(0);
        int i, size = postfix.size();
        Stack<BigDecimal> stack_num = new Stack<>();
        for (i = 0; i < size; i++) {
            if (postfix.get(i).getClass() == BigDecimal.class) {
                stack_num.push((BigDecimal) postfix.get(i));
            } else {
                BigDecimal a = stack_num.pop();
                BigDecimal b = stack_num.pop();
                switch ((Character) postfix.get(i)) {
                    case '+':
                        res = b.add(a);
                        break;
                    case '-':
                        res = b.subtract(a);
                        break;
                    case '*':
                        res = b.multiply(a);
                        break;
                    case '/':
                        res = b.divide(a, BigDecimal.ROUND_HALF_UP);
                        break;
                }
                stack_num.push(res);
            }
        }
        res = stack_num.pop();
        return res;
    }

    public void onEdit(View v) {
        if (dip.getVisibility() == View.VISIBLE) {
            dip.animate()
                    .alpha(0f)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            dip.setVisibility(View.GONE);
                        }
                    });
        } else {
            dip.setAlpha(0f);
            dip.setVisibility(View.VISIBLE);
            dip.animate()
                    .alpha(1f)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(null);
        }
    }
}
