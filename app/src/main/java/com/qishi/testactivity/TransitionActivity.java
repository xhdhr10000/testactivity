package com.qishi.testactivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Author: xhdhr10000
 * Date: 16/3/25
 */
public class TransitionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_transition);
    }

    public void onTransition(View v) {
        ImageView image = (ImageView) v;
        Intent intent = new Intent(this, AnimActivity.class);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, image, "image");
        startActivity(intent, options.toBundle());
    }
}
