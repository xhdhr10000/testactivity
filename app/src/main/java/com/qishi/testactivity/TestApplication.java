package com.qishi.testactivity;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by xhdhr10000 on 16/3/3.
 */

public class TestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        SugarContext.terminate();
        super.onTerminate();
    }
}
